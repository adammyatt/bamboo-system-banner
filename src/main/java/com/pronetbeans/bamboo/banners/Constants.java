package com.pronetbeans.bamboo.banners;

/**
 * Constants class.
 * 
 * @author Adam Myatt
 */
public class Constants {

    public static final String PREFIX = "custom.com.pronetbeans.bamboo.banners.";
    public static final String BANNER_SYSTEM_KEY = PREFIX + "system.header";
    
    
    public static String[] banner = {
        "______          __    _      _  ______  ",
        "| ___ \\        | \\ | |    | | | ___ \\ ",
        "| |_/ / __ ___  |  \\| | ___| |_| |_/ / ___  __ _ _ __  ___   ___ ___  _ __ ___ ",
        "|  __/ '__/ _ \\| . ` |/ _ \\ __| ___ \\/ _ \\/ _` | '_ \\/ __| / __/ _ \\| '_ ` _ \\ ",
        "| |  | | | (_) | |\\  |  __/ |_| |_/ /  __/ (_| | | | \\__ \\| (_| (_) | | | | | |",
        "\\_|  |_|  \\___/\\_| \\_/\\___|\\__\\____/ \\___|\\__,_|_| |_|___(_)___\\___/|_| |_| |_|"
    };
}
