package com.pronetbeans.bamboo.banners;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bandana.BandanaManager;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Adam Myatt
 */
public class BannerSysemHeaderServlet extends HttpServlet {

    private BandanaManager bandanaManager;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            response.setContentType("text/html;charset=utf-8");

            ServletOutputStream out = response.getOutputStream();

            String bannerText = getValueAsString(Constants.BANNER_SYSTEM_KEY);

            out.println("<div>" + bannerText + "</div>");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setBandanaManager(BandanaManager bandanaManager) {
        this.bandanaManager = bandanaManager;
    }

    public String getValueAsString(String key) {
        Object o = bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, key);
        return (String) o;

    }
}
