package com.pronetbeans.bamboo.banners;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bamboo.ww2.BambooActionSupport;
import com.atlassian.bandana.BandanaManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Adam Myatt
 */
public class BannersGlobalConfigurePlugin extends BambooActionSupport {

    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = Logger.getLogger(BannersGlobalConfigurePlugin.class);
    private BandanaManager bandanaManager;
    private String bannerSystem;

    /**
     * @return the bannerSystem
     */
    public String getBannerSystem() {
        return bannerSystem;
    }

    /**
     * @param bannerSystem the bannerSystem to set
     */
    public void setBannerSystem(String bannerSystem) {
        this.bannerSystem = bannerSystem;
    }

    @Override
    public String doInput() throws Exception {

        if (!super.hasGlobalAdminPermission()) {
            log.info("Currently logged in user does not have ADMIN permissions to perform this action.");
            return ERROR;
        }

        // load value(s) from Bandana storage into field for display in UI
        this.bannerSystem = getValueAsString(Constants.BANNER_SYSTEM_KEY);

        return INPUT;
    }

    @Override
    public String doExecute() throws Exception {

        if (!super.hasGlobalAdminPermission()) {
            log.info("Currently logged in user does not have ADMIN permissions to perform this action.");
            return ERROR;
        }

        // save all the value here
        setValue(Constants.BANNER_SYSTEM_KEY, this.bannerSystem);


        return SUCCESS;
    }

    public void setBandanaManager(BandanaManager bandanaManager) {
        this.bandanaManager = bandanaManager;
    }

    private void setValue(String key, Object obj) {
        bandanaManager.setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, key, obj);
    }

    public String getValueAsString(String key) {
        Object o = bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, key);
        if (o == null) {
            return "";
        } else {
            return (String) o;
        }
    }
}
