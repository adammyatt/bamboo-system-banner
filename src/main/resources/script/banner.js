var $systembanner = (function() {
    var headerBar = AJS.$("<div style='background-color:#003366'></div>");
    var bannerText;
    return {

        display: function() {
            headerBar.empty(); 
            var objDiv = AJS.$('#header');
            objDiv.prepend(headerBar);
            headerBar.append(AJS.$(this.getBannerText()));  
        },
        
        getBannerText: function() {
            AJS.$.ajax({
                url: window.BAMBOO.contextPath + "/plugins/servlet/bannercontent", 
                type: "GET", 
                async: false, 
                dataType: "html",
                success: function(data) {
                    bannerText = data;
                }
            })
            return bannerText;
        }        
    }
})();
 
AJS.toInit(function () {
    $systembanner.display();
});

